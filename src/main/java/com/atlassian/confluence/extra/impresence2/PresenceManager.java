package com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.extra.impresence2.reporter.PresenceReporter;

import java.util.Collection;

public interface PresenceManager
{
    PresenceReporter getReporter(String key);

    Collection<PresenceReporter> getReporters();
}
