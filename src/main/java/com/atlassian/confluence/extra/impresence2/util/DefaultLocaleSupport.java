package com.atlassian.confluence.extra.impresence2.util;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;

import java.util.List;

public class DefaultLocaleSupport implements LocaleSupport
{
    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    public DefaultLocaleSupport(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory)
    {
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    public String getText(final String key)
    {
        return getI18NBean().getText(key);
    }

    public String getText(final String key, final Object[] substitutions)
    {
        return getI18NBean().getText(key, substitutions);
    }

    public String getText(final String key, final List list)
    {
        return getI18NBean().getText(key, list);
    }

    public String getTextStrict(final String key)
    {
        return getI18NBean().getTextStrict(key);
    }
}
