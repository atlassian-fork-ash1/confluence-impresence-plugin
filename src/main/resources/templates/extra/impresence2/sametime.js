jQuery(function($) {
    $( "div[class='sametime-container']").each(function() {
        var getPath = function() {
            $( "input[name='sametimePath']").val();
        };

        var getLang = function() {
            $( "input[name='sametimeLang']").val();
        };

        var getUserName = function() {
            $( "input[name='sametimeUserId']").val();
        };

        setSTLinksURL(getPath(), getLang());

        writeSTLinksApplet ("", "", "");
        writeSametimeLink(getUserName(), getUserName(), true);
    });
});