package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.xml.sax.SAXException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;

public class SametimePresenceTestCase extends AbstractPresenceTestCase
{
    private String targetUser;

    protected void setUp() throws Exception
    {
        super.setUp();
        getBandanaHelper("extra.im.server.name.sametime").delete();
        targetUser = "john.doe@localhost.localdomain";
    }

    protected boolean requiresConfiguration()
    {
        return true;
    }

    public void testRequiresConfig()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Requires Configuration");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=sametime}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's IBM Sametime service before you can use this macro.");
        assertLinkPresentWithText("Configure IBM Sametime service");
    }

    public void testConfigurationNotAllowedForNonAdminUsers()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Dummy Account Configuration Accessible By Admins Only");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=sametime}"
        );

        assertTrue(pageHelper.create());

        logout();
        login(nonAdminUserName, nonAdminPassword);
        try
        {
            gotoPage("/");
            gotoPage("/display/" + testSpaceKey);

            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextPresent("An administrator must configure your server's IBM Sametime service before you can use this macro.");
            assertLinkNotPresentWithText("Configure IBM Sametime service");
        }
        finally
        {
            logout();
            loginAsAdmin(); /* So tearDown can be called successfully */
        }
    }

    private void configureSametimeServer(final String serverLocation)
    {
        setWorkingForm("configuresametimeform");
        setTextField("server", serverLocation);
        submit("update");

        /* Assert if the values are correctly set */
        assertTitleEquals("Configure IBM Sametime service - Confluence");
        assertThat(getElementAttributeByXPath("//input[@name='server']", "value"), is(serverLocation));
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=sametime}"
        );

        assertTrue(pageHelper.create());

        try
        {
            // Configure IBM Sametime service requires escalated privileges
            gotoPageWithEscalatedPrivileges("/pages/viewpage.action?pageId=" + pageHelper.getId());

            clickLinkWithText("Configure IBM Sametime service");

            assertTitleEquals("Configure IBM Sametime service - Confluence");
            assertThat(getElementAttributeByXPath("//input[@name='server']", "value"), isEmptyString());

            configureSametimeServer("localhost.localdomain");

        	/* Now, let's see if the page shows the presence of the targeted user */
            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextNotPresent("An administrator must configure your server's IBM Sametime service before you can use this macro.");
        }
        finally
        {
            dropEscalatedPrivileges();
            assertTrue(getBandanaHelper("extra.im.server.name.sametime").delete());
        }
    }
}
