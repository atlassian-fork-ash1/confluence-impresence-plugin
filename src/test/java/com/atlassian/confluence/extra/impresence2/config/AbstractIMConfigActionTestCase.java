package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.impresence2.PresenceManager;
import com.atlassian.confluence.extra.impresence2.reporter.PresenceReporter;
import com.atlassian.confluence.extra.impresence2.PresenceReporterModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import junit.framework.TestCase;
import org.mockito.Mock;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

public abstract class AbstractIMConfigActionTestCase<A extends ConfluenceActionSupport, T extends PresenceReporter> extends TestCase
{
    @Mock
    ContainerContext containerContext;

    @Mock
    BandanaManager bandanaManager;

    @Mock
    PluginAccessor pluginAccessor;

    @Mock
    PresenceReporterModuleDescriptor presenceReporterModuleDescriptor;

    @Mock
    PresenceManager presenceManager;

    A action;

    T presenceReporter;

    @Mock
    PresenceReporter presenceReporterReplaced;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(containerContext.getComponent("bandanaManager")).thenReturn(bandanaManager);

        ContainerManager.getInstance().setContainerContext(containerContext);

        presenceReporter = createReporter();
        when(presenceManager.getReporter(getServiceKey())).thenReturn(presenceReporter);

        when(presenceReporterModuleDescriptor.getKey()).thenReturn(getServiceKey());
        when(presenceReporterModuleDescriptor.getModule()).thenReturn(presenceReporter);

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(PresenceReporterModuleDescriptor.class)).thenReturn(
                Arrays.asList(presenceReporterModuleDescriptor)
        );
        
        action = createAction();
    }

    protected void tearDown() throws Exception
    {
        ContainerManager.getInstance().setContainerContext(null);
        super.tearDown();
    }

    protected abstract A createAction();

    protected abstract T createReporter();

    protected abstract String getServiceKey();

    public void testExecuteWhenReporterIsInvalid() throws Exception
    {
        when(presenceManager.getReporter(anyString())).thenReturn(null);

        assertEquals("error", (action = createAction()).execute());
        assertTrue(action.hasErrors());
    }
}
