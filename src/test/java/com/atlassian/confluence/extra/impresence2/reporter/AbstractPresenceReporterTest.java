package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.extra.impresence2.util.LocaleSupport;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.spring.container.ContainerContext;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

public abstract class AbstractPresenceReporterTest<T extends PresenceReporter> extends TestCase
{
    @Mock
    BootstrapManager bootstrapManager;

    @Mock
    BandanaManager bandanaManager;

    @Mock
    ContainerContext containerContext;

    LocaleSupport localeSupport;

    T presenceReporter;

    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        localeSupport = new LocaleSupport()
        {
            public String getText(final String key)
            {
                return key;
            }

            public String getText(final String key, final Object[] substitutions)
            {
                return key;
            }

            public String getText(final String key, final List substitutions)
            {
                return key;
            }

            public String getTextStrict(final String key)
            {
                return key;
            }
        };

        presenceReporter = createPresenceReporter();
    }

    protected void tearDown() throws Exception
    {
        bootstrapManager = null;
        bandanaManager = null;
        containerContext = null;
        localeSupport = null;
        presenceReporter = null;
        super.tearDown();
    }

    protected abstract String getPresenceReporterKey();

    protected abstract T createPresenceReporter();
}